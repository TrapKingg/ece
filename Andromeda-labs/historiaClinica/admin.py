from django.contrib import admin
from . import models
# Register your models here.
admin.site.register(models.Paciente)
admin.site.register(models.Antecedentes)
admin.site.register(models.AntecedentesHeredofamiliares)
admin.site.register(models.antecedentesPersonales)
admin.site.register(models.Fisiologicos)
admin.site.register(models.Patologicos)
admin.site.register(models.signosVitales)
