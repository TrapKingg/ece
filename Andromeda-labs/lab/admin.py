from django.contrib import admin
from .models import EstudioList, Estudio
# Register your models here.

admin.site.register(EstudioList)
admin.site.register(Estudio)
