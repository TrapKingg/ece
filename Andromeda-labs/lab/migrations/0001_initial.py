# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2018-01-19 04:48
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('historiaClinica', '0004_auto_20180103_2041'),
    ]

    operations = [
        migrations.CreateModel(
            name='Estudio',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nEstudio', models.CharField(blank=True, max_length=200, null=True, verbose_name='Enfermedad')),
                ('estudioDetalle', models.TextField(blank=True, null=True, verbose_name='Detalle del estudio')),
                ('nurseNotes', models.TextField(blank=True, max_length=200, null=True, verbose_name='Notas (observaciones)')),
                ('is_normal', models.BooleanField(default=False)),
                ('is_danger', models.BooleanField(default=False)),
                ('is_good', models.BooleanField(default=False)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('paciente', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='historiaClinica.Paciente')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
