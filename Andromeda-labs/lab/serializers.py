from rest_framework import serializers
from .models import Estudio, EstudioList
from django.contrib.auth.models import User

class estudioSerializer(serializers.ModelSerializer):
    user = serializers.ReadOnlyField(source='user.username')

    class Meta:
        model = Estudio
        fields = ('user', 'paciente', 'nEstudio', 'estudioDetalle', 'nurseNotes', 'is_good', 'is_normal', 'is_danger', )

class estudioListSerializer(serializers.ModelSerializer):

    class Meta:
        model = EstudioList
        fields = ('id', 'name', 'description', )
