from django.shortcuts import render, get_object_or_404, redirect
from .forms import userRegistration
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import View
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib import messages
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
# Create your views here.
# class uRegisterView(View):

#     def get(self, request, *args, **kwargs):
#         form = userRegistration()
#         return render(request, 'registration/register.html', {'form':form})
    
#     def post(self, request, *args, **kwargs):
#         form = userRegistration(request.POST or None)
#         if form.is_valid():
#             #creamos un nuevo objeto usuario
#             new_user = form.save(commit=False)
#             #insertamos la contraseña
#             new_user.set_password(form.cleaned_data['password'])
#             #guardamos el objeto usuario
#             new_user.save()
#             #retornamos la url de éxito o un html
#             messages.success(request, '¡Se ha registrado exitosamente!')
#             return render(request, 'registration/register_done.html', {'form':form})

def uRegisterView(request):
    if request.user.is_authenticated:
        return HttpResponseRedirect('/')
    else:
        if request.method == 'POST':
            form = userRegistration(request.POST or None)
            if form.is_valid():
                new_user = form.save(commit=False)
                new_user.set_password(form.cleaned_data['password1'])
                new_user.save()
                messages.success(request, '¡Se ha registrado exitosamente!')
                return render(request, 'registration/register_done.html', {'form':form})
        else:
            form = userRegistration()
        return render(request, 'registration/register.html', {'form':form})

from django.core.exceptions import ObjectDoesNotExist

@login_required
def userProfile(request, pk):

    try:
        user = get_object_or_404(User, pk=pk)
        return render(request, 'profile/profile.html', {'user':user})
    except ObjectDoesNotExist:
        raise http404
        #return render(request, 'extra/404.html')

