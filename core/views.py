from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.decorators import login_required
from django.views import generic

from django.http import JsonResponse
from django.contrib import messages
from django.utils.decorators import method_decorator
from historiaClinica import models as modelsHC

from historiaClinica.forms import pacienteForm
from historiaClinica.serializers import pacienteSerializer, antecedenteSerializer
from .permissions import IsOwnerOrReadOnly
from .mixins import UserOwnerMixin, FormUserNeededMixin, LoginRequiredMixin

from rest_framework import generics
from rest_framework.permissions import IsAuthenticated

# Create your views here.

@login_required
def dashboard(request):
    return render(request, 'core/dashboard.html', {'section':'index'})

class historiaClinicaView(LoginRequiredMixin, generic.View):

    def get(self, request, *args, **kwargs):
        pacientes = modelsHC.Paciente.objects.all()
        return render(request, 'hiscli/index.html', {'pacientes':pacientes, 'section':'hiscli'})

from django.template.loader import render_to_string

class pacientesList(generics.ListCreateAPIView):
    queryset = modelsHC.Paciente.objects.all()
    serializer_class = pacienteSerializer
    permission_classes = (IsAuthenticated, )

    def list(self, request):
        queryset = self.get_queryset()
        serializer = pacienteSerializer(queryset, many=True)
        return Response(serializer.data)
    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status, permissions
from django.http import Http404
# class pacientesList(APIView):
#     """
#     Lista todos los pacientes o crea uno nuevo
#     """
#     def get(self, request, format=None):
#         pacientes = Paciente.objects.all()
#         serializer = pacienteSerializer(pacientes, many=True)
#         return Response(serializer.data)

#     def post(self, request, format=None):
#         serializer = pacienteSerializer(data=request.data)
#         if serializer.is_valid():
#             serializer.save(user=self.request.user)
#             return Response(serializer.data, status=status.HTTP_201_CREATED)
#         return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

# Inician las vistas del paciente
class pacienteDetail(APIView):

    def get_object(self, pk):
        try:
            return modelsHC.Paciente.objects.get(pk=pk)
        except modelsHC.Paciente.DoesNotExist:
            raise Http404
    def get(self, request, pk, format=None):
        paciente = self.get_object(pk)
        serializer = pacienteSerializer(paciente)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        paciente = self.get_object(pk)
        serializer = pacienteSerializer(paciente, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        paciente = self.get_object(pk)
        paciente.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
# TERMINAN VISTAS DEL PACIENTE
# INCIA VISTAS DE ANTECEDENTES DEL PACIENTE
@login_required
def pacienteDetail2(request, pk=None):
    try:
        paciente = None
        paciente = get_object_or_404(modelsHC.Paciente, pk=pk)
    except modelsHC.Paciente.DoesNotExist:
        pass
    try:
        antecedentes = None
        antecedentes = modelsHC.Antecedentes.objects.get(paciente=paciente)
    except modelsHC.Antecedentes.DoesNotExist:
        pass
    try:
        ahf = None
        ahf = modelsHC.AntecedentesHeredofamiliares.objects.get(paciente=paciente)
    except modelsHC.AntecedentesHeredofamiliares.DoesNotExist:
        pass
    try:
        ap = None
        ap = modelsHC.antecedentesPersonales.objects.get(paciente=paciente)
    except modelsHC.antecedentesPersonales.DoesNotExist:
        pass
    try:
        fisio = None
        fisio = modelsHC.Fisiologicos.objects.get(antecedente=ap)
    except modelsHC.Fisiologicos.DoesNotExist:
        pass
    try:
        pato = None
        pato =modelsHC.Patologicos.objects.get(antecedente=ap)
    except modelsHC.Patologicos.DoesNotExist:
        pass
    try:
        signos = None
        signos = modelsHC.signosVitales.objects.get(paciente=paciente)
    except modelsHC.signosVitales.DoesNotExist:
        pass
    return render(request, 'hiscli/paciente_detail.html', {'paciente':paciente,
                                            'antecedentes':antecedentes,
                                            'ahf':ahf,
                                            'ap':ap, 'fisio':fisio, 'pato':pato,
                                            'signos':signos})
# agregar y listar antecedentes
class antecedenteList(APIView):
    """
    Lista todos los antecedentes o crea uno nuevo
    """
    def get_object(self, pk):
        try:
            paciente = modelsHC.Paciente.objects.get(pk=pk)
            return paciente
        except modelsHC.Antecedentes.DoesNotExist:
            raise Http404
    def get(self, request, pk, format=None):
        paciente = self.get_object(pk)
        antecedentes = paciente.antecedentes_set.all()
        serializer = antecedenteSerializer(antecedentes, many=True)
        return Response(serializer.data)
