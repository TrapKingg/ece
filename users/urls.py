from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^accounts/register/$', views.uRegisterView, name='user_register'),
    url(r'^accounts/profile/(?P<pk>\d+)/$', views.userProfile, name='user_profile'),
]