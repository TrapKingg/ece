from django.db import models
from core.models import TimeStampedModel
from django.conf import settings
from django.core.urlresolvers import reverse
# Create your models here.
GENDER_CHOICES = (
    ('M', 'Masculino'),
    ('F', 'Femenino'),
)
CIVIL_STATUS_CHOICES = (
    ('C', 'Casado(a)'),
    ('S', 'Soltero(a)'),
    ('D', 'Divorciado(a)'),
)
class userProfile(TimeStampedModel):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    address = models.CharField('Dirección', max_length=100)
    phone_number = models.CharField('Número de telefono', max_length=13)
    gender = models.CharField('Sexo', choices=GENDER_CHOICES, max_length=15)
    birth_day = models.DateField('Fecha de nacimiento', blank=True, null=True)
    matricula = models.CharField('Cedula/licencia de trabajo', max_length=25)
    civil_state = models.CharField('Estado civil', max_length=20, choices=CIVIL_STATUS_CHOICES)
    country = models.CharField('Pais', max_length=40)
    city = models.CharField('Ciudad', max_length=60)
    location = models.CharField('Poblado/Municipio', max_length=100)
    Nacionality = models.CharField('Nacionalidad', max_length=60)
    especialidad = models.CharField('Especialidad', max_length=100, blank=True, null=True)
    especialidad1 = models.CharField('Especialidad', max_length=100, blank=True, null=True)
    especialidad2 = models.CharField('Especialidad', max_length=100, blank=True, null=True)
    is_doctor = models.BooleanField('¿Es médico?', default=False)
    is_nurse = models.BooleanField('¿Es enfermera?', default=False)

    def __str__(self):
        return 'Perfil de - %s' % (self.user.get_full_name())
    
    def get_absolute_url(self):
        if not self.matricula:
            return reverse("user_profile", kwargs={'pk': self.user.pk})
        return reverse('user_profile', kwargs={'matricula': self.matricula})
        

    def set_doctor(self):
        self.is_doctor = True
        self.is_nurse = False
        self.save()
    def set_nurse(self):
        self.is_doctor = False
        self.is_nurse = True
        self.save()

